from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import (
  GenericRelation, GenericForeignKey)
from taggit.managers import TaggableManager
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _


class Vote(models.Model):
    """Model class to host every vote, made with ContentType framework to
    allow a single model connected to Questions and Answers."""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    value = models.BooleanField(default=True)
    content_type = models.ForeignKey(ContentType,
        blank=True, null=True, related_name="votes_on", on_delete=models.CASCADE)
    object_id = models.CharField(
        max_length=50, blank=True, null=True)
    vote = GenericForeignKey(
        "content_type", "object_id")

    class Meta:
        verbose_name = _("Vote")
        verbose_name_plural = _("Votes")
        index_together = ("content_type", "object_id")
        unique_together = ("user", "content_type", "object_id")

class Reddit(models.Model):
    created_by = models.ForeignKey(
      settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, unique=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    image = models.ImageField(
      ('Featured image'), blank=True)
    votes = GenericRelation(Vote)

class Comment(models.Model):
    created_by = models.ForeignKey(
      settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    reddit = models.ForeignKey(
         Reddit, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    reply_to = models.ForeignKey(
      'self', on_delete=models.CASCADE, null=True)




class Question(models.Model):
    title = models.CharField(max_length=1000, unique=True)
    description = models.TextField()
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    tags = TaggableManager()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('id', )


class Answer(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    content = models.TextField()
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.content

    class Meta:
        unique_together = ('question', 'user')
        ordering = ('id', )
