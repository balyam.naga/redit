from rest_framework import serializers
from .models import Question, Answer


class AnswerSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Answer
        fields = ('user', 'question', 'content', 'votes')


class QuestionSerializer(serializers.ModelSerializer):

    answers = AnswerSerializer(many=True, read_only=True)

    class Meta(object):
        model = Question
        fields = ('id', 'title', 'description', 'answers')
        
class AnswerDetailSerializer(serializers.ModelSerializer):
    question = serializers.HyperlinkedRelatedField(
        lookup_field='pk',
        read_only=True,
        view_name='question-detail', )

    class Meta(object):
        model = Answer
        fields = ('user', 'question', 'content', 'votes')
