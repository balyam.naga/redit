from rest_framework import generics, permissions
from qa.models import Question, Answer
from qa.serializers import (
    QuestionSerializer, AnswerDetailSerializer)
from qa.permissions import IsOwnerOrReadOnly



class QuestionList(generics.ListCreateAPIView):
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,)
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsOwnerOrReadOnly,)
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class AnswerList(generics.ListCreateAPIView):
    permission_classes = (IsOwnerOrReadOnly,)
    serializer_class = AnswerDetailSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Answer.objects.filter(question=pk)
